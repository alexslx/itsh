/*
 * itsh - ITEE Shell
 *
 * @author Alexandre Leites <alexslx@live.com>
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <ctype.h>

/**
 * Cleanup spaces at beginning or end of the string,
 * and also remove consecutive spaces.
 */
char* utils_specialtrim(char* str)
{
	char* result;
	char* end;
	int i=0, j=0;

	result = (char *)malloc( strlen(str) + 1);

	result[0] = '\0';

	while(*(str+i))
	{
		if ( !(isspace(*(str+i)) && isspace(*(str+i+1))) )
		{
			*(result+j) = *(str+i);
			j++;
		}
		i++;
	}
	*(result+j) = '\0';

	// Remove 1st space
	while(isspace(*result))
		result++;

	// Check if the string was just spaces
	if(*result == 0)
		return result;

	// Remove last space
	end = result + strlen(result) - 1;
	while(end > result && isspace(*end))
		end--;

	*(end+1) = 0;

	return result;
}
