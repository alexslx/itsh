/*
 * itsh - ITEE Shell
 *
 * @author Alexandre Leites <alexslx@live.com>
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <fcntl.h>
#include <string.h>

#include <sys/wait.h>

#include "job.h"
#include "command.h"

/**
 * Initialize and reset job structure
 */
void job_init(job_t* j)
{
	j->cmds = NULL;
	j->jobc = 0;
}

/**
 * Add a command to the job
 */
void job_addcmd(job_t* j, command_t* c)
{
	j->cmds = (command_t **) realloc(j->cmds,  (j->jobc+1) * sizeof(command_t *) );
	j->cmds[j->jobc] = c;
	j->jobc++;
}

/**
 * Check if the job should run in background.
 * The last command determine it.
 */
bool job_isbackground(job_t* j)
{
	if(j->jobc > 0 )
		return j->cmds[j->jobc-1]->is_background;

	return false;
}

/**
 * Check if the background job is finished.
 */
bool job_checkbackground(job_t* j)
{
	pid_t pid;
	int status;
	command_t* c;

	if(j->jobc < 1)
		return true;

	c = j->cmds[j->jobc-1];
	if(c->status > RUNNING)
		return true;

	pid = c->pid;
	pid = waitpid(pid, &status, WNOHANG | WUNTRACED);
	if(pid > 0)
	{
		printf("* Done [%d] => %d\n", pid, status);
		fflush(NULL);

		c->rc = status;
		if(WIFSTOPPED(status))
			c->status = STOPPED;
		else
			c->status = FINISHED;

		return true;
	}

	return false;
}

/**
 * Check if any of the commands still running.
 */
bool job_checkwait(job_t* j)
{
	int i;
	command_t* c;

	for(i=0; i < j->jobc; i++)
	{
		c = j->cmds[i];

		if(c->status != STOPPED && c->status != FINISHED)
			return true;
	}

	return false;
}

/**
 * Wait for the foreground finish.
 */
void job_wait(job_t* j)
{
	pid_t pid;
	int status;
	command_t* c;
	int i;

	while(job_checkwait(j))
	{
		pid = waitpid(WAIT_ANY, &status, WUNTRACED);
		if(pid > 0)
		{
			for(i=0; i < j->jobc; i++)
			{
				c = j->cmds[i];
				if(c->pid != pid)
					continue;

				c->rc = status;
				if(WIFSTOPPED(status))
					c->status = STOPPED;
				else
					c->status = FINISHED;
			}
		}
	}
}

/**
 * Get last return code
 */
int job_getrc(job_t* j)
{
	command_t* c;

	if(j->jobc < 1)
		return EXIT_SUCCESS;

	c = j->cmds[j->jobc-1];
	if(c == NULL)
		return EXIT_FAILURE;

	return c->rc;
}

/**
 * Run the command.
 */
int job_run(job_t* j, bool background)
{
	pid_t pid;
	command_t* c;
	int i;
	int rc;
	int job_input;
	int job_output;
	int job_err;

	int job_pipe[2];

	job_input  = STDIN_FILENO;
	job_output = STDOUT_FILENO;
	job_err    = STDERR_FILENO;
	rc         = EXIT_SUCCESS;

	for(i=0; i < j->jobc; i++)
	{
		c = j->cmds[i];

		// If there is another command in the queue, pipe it
		if(i != (j->jobc-1))
		{
			if(pipe(job_pipe) != 0)
			{
				perror("job pipe error");
				return EXIT_FAILURE;
			}

			job_output = job_pipe[1];
		}

		// Buildin commands
		if(strcmp(c->name, "exit") == 0)
		{
			exit(EXIT_SUCCESS);
		}
		else if(strcmp(c->name, "cd") == 0)
		{
			int rc;
			char* path = NULL;
			if(c->argc > 1)
				path = c->argv[1];

			if(path == NULL)
				path = getenv("HOME");

			rc = chdir(path);
			if(rc == -1)
			{
				c->status = STOPPED;
				perror(path);
				return rc;
			}
			else
				c->status = FINISHED;
		}
		else
		{
			// Utilize fork to run the command
			pid = fork();
			switch(pid)
			{
				case -1:
					perror("error while forking");
					return EXIT_FAILURE;
				break;

				case 0:
					if(i == (j->jobc-1))
					{
						// Output direction
						if(c->output != NULL)
							job_output = open(c->output, O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH); // open with w, or create with rw-rw-r
						else if(background)
							job_output = open("/dev/null", O_WRONLY | O_CREAT | O_TRUNC, S_IRUSR | S_IWUSR | S_IRGRP | S_IROTH);
						else
							job_output = STDOUT_FILENO;

						if(job_output < 0)
						{
							perror("error while opening output file");
							return EXIT_FAILURE;
						}
					}

					// just the 1st might have input redirection
					if(i == 0)
					{
						if(c->input != NULL)
							job_input = open(c->input, O_RDONLY);
						else if(background)
							job_input = open("/dev/null", O_RDONLY);
						else
							job_input = STDIN_FILENO;

						if(job_input < 0)
						{
							perror("error while opening input file");
							return EXIT_FAILURE;
						}
					}

					command_dojob(c, j->group_id, job_input, job_output, job_err, background);
					c->status = RUNNING;
				break;

				default:
					c->pid = pid;
					if(!j->group_id)
						j->group_id = pid;

					setpgid(pid, j->group_id);
				break;
			}
		}

		// Close the file descriptors
		if(j->jobc > 1)
		{
			if(i > 0)
				close(job_input);

			if(i < (j->jobc-1))
				close(job_output);
		}

		job_input = job_pipe[0];
	}

	if(background)
	{
		// If executed a background process, print its process id (pid).
		printf("%d\n", pid);
		rc = EXIT_SUCCESS;
	}
	else
	{
		job_wait(j);
		rc = job_getrc(j);
	}

	return rc;
}
