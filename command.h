/*
 * itsh - ITEE Shell
 *
 * @author Alexandre Leites <alexslx@live.com>
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef COMMAND_H_
#define COMMAND_H_

#include <stdbool.h>

typedef enum {NONE, RUNNING, STOPPED, FINISHED} cmd_status;

typedef struct command {
	pid_t pid;
	char* name;
	char* input;
	char* output;
	char** argv;
	int argc;
	int rc;
	bool is_background;
	cmd_status status;
} command_t;

void command_init(command_t* c);
void command_print(command_t* c);

void command_dojob(command_t* c, pid_t group_id, int fdin, int fdout, int fderr, bool background);

command_t* command_parse(char* commandstr);

#endif /* COMMAND_H_ */
