/*
 * itsh - ITEE Shell
 *
 * @author Alexandre Leites <alexslx@live.com>
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include <stdio.h>
#include <stdlib.h>

#include <readline/readline.h>
#include <readline/history.h>

#include "terminal.h"

int running;
terminal_t term;

static void input_handler(char* line)
{
	rl_callback_handler_remove ();

	if(line == NULL)
	{
		printf("\n");
		running = 0;
		return;
	}

	add_history(line);

	terminal_parseline(&term, line);
	terminal_buildpromptline(&term);
	terminal_checkjobs(&term);

	rl_callback_handler_install(term.prompt, input_handler);

	free(line);
}

int main(int argc, char *argv[])
{
	int rc, rs;
	fd_set fds;

	terminal_init(&term);
	terminal_build(&term);

	rc = EXIT_SUCCESS;
	running = 1;

	terminal_buildpromptline(&term);
	rl_callback_handler_install(term.prompt, input_handler);

    while(running)
    {
    	FD_ZERO (&fds);
    	FD_SET (fileno (rl_instream), &fds);

		rs = select (FD_SETSIZE, &fds, NULL, NULL, NULL);
		if (rs < 0)
		{
			perror ("rltest: select with size zero");
			rl_callback_handler_remove ();
			break;
		}

		if (FD_ISSET(fileno(rl_instream), &fds))
			rl_callback_read_char ();
    }

    return rc;
}
