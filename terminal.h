/*
 * itsh - ITEE Shell
 *
 * @author Alexandre Leites <alexslx@live.com>
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef TERMINAL_H_
#define TERMINAL_H_

#include <sys/types.h>
#include <termios.h>
#include <stdbool.h>

#include "job.h"

typedef struct terminal {
	char user[256];
	char dir[256];
	char* prompt;

	struct {
		int shell_id;
		pid_t group_id;
	} ids;

	job_t** jobs;
	int jobc;

	struct termios tmodes;
} terminal_t;

void terminal_init(terminal_t* term);
void terminal_build(terminal_t* term);
void terminal_buildpromptline(terminal_t* term);
int  terminal_parseline(terminal_t* term, char* line);

char* terminal_readline(terminal_t* term);

void terminal_addjob(terminal_t* t, job_t* j);
void terminal_checkjobs(terminal_t* t);

#endif /* TERMINAL_H_ */
