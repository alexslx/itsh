/*
 * itsh - ITEE Shell
 *
 * @author Alexandre Leites <alexslx@live.com>
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#ifndef JOB_H_
#define JOB_H_

#include <stdbool.h>

#include "command.h"

typedef struct job {
	pid_t group_id;
	command_t** cmds;
	int jobc;
} job_t;

void job_init(job_t* j);
void job_addcmd(job_t* j, command_t* c);

bool job_checkbackground(job_t* j);
bool job_isbackground(job_t* j);

int job_run(job_t* j, bool background);

bool job_checkwait(job_t* j);
void job_wait(job_t* j);
int job_getrc(job_t* j);

#endif /* JOB_H_ */
