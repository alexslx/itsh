/*
 * itsh - ITEE Shell
 *
 * @author Alexandre Leites <alexslx@live.com>
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>

#include <pwd.h>
#include <signal.h>

#include "terminal.h"
#include "command.h"
#include "job.h"
#include "utils.h"

/**
 * Initialize and reset terminal structure memory
 */
void terminal_init(terminal_t* term)
{
	memset(term->user, 0, sizeof(term->user));
	memset(term->dir, 0, sizeof(term->dir));
	term->prompt = NULL;
	term->jobs = NULL;
    term->jobc = 0;
	term->ids.shell_id = STDIN_FILENO;
	term->ids.group_id = getpgrp();

	// Ignore all signals
	signal(SIGINT,  SIG_IGN);
	signal(SIGQUIT, SIG_IGN);
	signal(SIGTSTP, SIG_IGN);
	signal(SIGTTIN, SIG_IGN);
	signal(SIGTTOU, SIG_IGN);

	term->ids.group_id = getpid();
	if(setpgid(term->ids.group_id, term->ids.group_id) != 0)
	{
		perror("error while setting the group id");
		exit(EXIT_FAILURE);
	}

	// Save terminal state for later
	tcsetpgrp(term->ids.shell_id, term->ids.group_id);
	tcgetattr(term->ids.shell_id, &term->tmodes);
}

/**
 * Populate terminal structure with current user and directory
 */
void terminal_build(terminal_t* term)
{
	char buf[512];
	char dir[256];

	struct passwd* pwd;

	memset(&buf, 0, sizeof(buf));
	memset(&dir, 0, sizeof(dir));

	getcwd(dir, sizeof(dir));
	pwd = getpwuid(getuid());

	if(pwd)
		strncpy(term->user, pwd->pw_name, sizeof(term->user));

	strncpy(term->dir, dir, sizeof(term->dir));
}

/**
 * Build the prompt line with the user and directory
 * eg. 'alexslx:/home/alexslx# '
 */
void terminal_buildpromptline(terminal_t* term)
{
	int size;

	if(term->prompt != NULL)
	{
		free(term->prompt);
		term->prompt = NULL;
	}

	terminal_build(term); //TODO: maybe should I just do that when is really required?
	size = strlen(term->user) + strlen(term->dir) + 5;
	term->prompt = (char *)malloc(size * sizeof(char));

	snprintf(term->prompt, size-1, "%s:%s# ", term->user, term->dir);
}

/**
 * Add background job to verification queue
 */
void terminal_addjob(terminal_t* t, job_t* j)
{
	t->jobs = (job_t **) realloc(t->jobs,  (t->jobc+1) * sizeof(job_t *) );
	t->jobs[t->jobc] = j;
	t->jobc++;
}

/**
 * Check background jobs
 */
void terminal_checkjobs(terminal_t* t)
{
	int i;
	for(i=0; i < t->jobc; i++)
	{
		job_checkbackground(t->jobs[i]);
	}
}

/**
 * Parse one line and split into commands (if there's a pipe).
 */
int terminal_parseline(terminal_t* term, char* line)
{
	char* str;
	char* command;
	char* command_r;
	int rc;

	str = utils_specialtrim(line);

	if(str == NULL || str[0] == '#')
		return EXIT_SUCCESS;

	// Each string separated by ';' is a new command.
	command = strtok_r(str, ";", &command_r);
	while(command != NULL)
	{
		char* single_command;
		char* single_command_r;
		job_t* j;
		bool isbackground;

		j = (job_t*) malloc(sizeof(job_t *));
		job_init(j);

		// Piping
		single_command = strtok_r(command, "|", &single_command_r);
		while(single_command != NULL)
		{
			command_t* c = command_parse(single_command);

			job_addcmd(j, c);

			single_command = strtok_r(NULL, "|", &single_command_r);
		}

		command = strtok_r(NULL, ";", &command_r);

		isbackground = job_isbackground(j);

		rc = job_run(j, isbackground);
		if(isbackground)
		{
			terminal_addjob(term, j);
			continue;
		}

		tcsetpgrp(term->ids.shell_id, j->group_id);
	}

	// Return terminal state after foreground execution.
	tcsetpgrp(term->ids.shell_id, term->ids.group_id);
	tcsetattr(term->ids.shell_id, TCSADRAIN, &term->tmodes);

	return rc;
}
