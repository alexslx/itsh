/*
 * itsh - ITEE Shell
 *
 * @author Alexandre Leites <alexslx@live.com>
 *
 * Licensed under The MIT License
 * For full copyright and license information, please see the LICENSE.txt
 * Redistributions of files must retain the above copyright notice.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 */

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <string.h>
#include <signal.h>

#include "command.h"
#include "utils.h"

/**
 * Initialize and reset command structure memory
 */
void command_init(command_t* c)
{
	c->name = NULL;
	c->argv = NULL;
	c->input = NULL;
	c->output = NULL;
	c->is_background = false;
	c->argc = 0;
	c->rc = 0;
	c->status = NONE;
}

/**
 * Debug function
 */
void command_print(command_t* c)
{
	int i;
	if(c == NULL)
	{
		printf("* COMMAND IS NULL!\n");
		return;
	}

	printf("* Name: %s\n", c->name);
	printf("* * Input: %s\n", c->input ? c->input : "Default");
	printf("* * Output: %s\n", c->output ? c->output : "Default");
	printf("* * Background: %s\n", c->is_background ? "Yes":"No");
	printf("* * Return Code: %d\n", c->rc);
	if(c->argc > 0)
	{
		printf("* * Arguments:\n");
		for(i=0; i < c->argc; i++)
			printf("* * * %d: %s\n", i, c->argv[i]);
	}
}

/**
 * Parse one command and build a command structure.
 */
command_t* command_parse(char* commandstr)
{
	command_t* c;
	char* arg;
	char* arg_r;

	c = (command_t*) malloc(sizeof(command_t));
	command_init(c);

	commandstr = utils_specialtrim(commandstr);

	if(commandstr[strlen(commandstr) -1] == '&')
		c->is_background = true;

	// Parse arguments
	arg = strtok_r(commandstr, " ", &arg_r);
	while(arg != NULL)
	{
		int avoid_args = 0;

		if(c->name == NULL)
			c->name = arg;

		if(arg[0] == '<' || arg[0] == '>')
		{
			// I/O redirection detection
			char* filename 	= strtok_r(NULL, " ", &arg_r);
			avoid_args 		= 1;

			if(filename == NULL)
			{
				continue;
			}

			if(arg[0] == '<')
				c->input = filename;
			else
				c->output = filename;

		}
		else if(avoid_args == 0)
		{
			// Background job detection
			if(arg[0] == '&')
			{
				avoid_args = 1;
				arg = strtok_r(NULL, " ", &arg_r);

				continue;
			}

			c->argv = (char **) realloc(c->argv,  (c->argc+1) * sizeof(char *) );
			c->argv[c->argc] = arg;
			c->argc++;
		}

		arg = strtok_r(NULL, " ", &arg_r);
	}

	// Commands without arguments should have a second "NULL" argument in execvp
	c->argv = (char **) realloc(c->argv,  (c->argc+1) * sizeof(char *) );
	c->argv[c->argc] = NULL;
	c->argc++;

	return c;
}

/**
 * Execute the command
 */
void command_dojob(command_t* c, pid_t group_id, int fdin, int fdout, int fderr, bool background)
{
	pid_t pid = getpid();

	if(!group_id)
		group_id = pid;

	setpgid(pid, group_id);

	if(!background)
		tcsetpgrp(STDIN_FILENO, group_id);

	// unblock signals
	signal(SIGTTOU, SIG_DFL);
	signal(SIGINT,  SIG_DFL);
	signal(SIGCHLD, SIG_DFL);
	signal(SIGTTIN, SIG_DFL);
	signal(SIGQUIT, SIG_DFL);
	signal(SIGTSTP, SIG_DFL);

	// Copy and close the file descriptors
	if (fdin != STDIN_FILENO)
	{
		dup2(fdin, STDIN_FILENO);
		close(fdin);
	}

	if (fdout != STDOUT_FILENO)
	{
		dup2(fdout, STDOUT_FILENO);
		close(fdout);
	}

	if (fderr != STDERR_FILENO)
	{
		dup2(fderr, STDERR_FILENO);
		close(fderr);
	}

	execvp(c->name, c->argv);

	// If any error executing happens
	perror(c->name);
	exit(EXIT_FAILURE);
}
