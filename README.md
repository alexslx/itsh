itsh 1 "AUGUST 2014" Linux "itsh - ITEE Shell (Command Interpreter)"
====================================================================


NAME
----
itsh - ITEE Shell (Command Interpreter)


SYNOPSIS
--------
itsh is a command language interpreter that executes commands read from the standard input or from a file.
	
DESCRIPTION
-----------
itsh is a command line interpreter that execute commands both from standard input or from files. The purpose of this program is to get familiar with unix processes, creating them, passing arguments, inheriting open ﬁles, waiting for them to terminate, background processes, and so on. This program was been written in C using GNU readline library and C99 extensions.

itsh allows the user to execute commands in the same way as bash does. It is possible to pipe (execute and connect a series of commands) and execute these commands both in foreground (making the terminal hang and wait for the job processing) or run the commands in background (running the job in a separated process, allowing the user to continue typing new commands). Also, it is possible to control how the commands will receive or display information for the user by being able to redirect the input and/or output of these commands, allowing to recovery or store information from/to files.

In order to exit the itsh, it is possible to type the "**exit**" command, without any arguments (arguments will be ignored).

Also, it is possible to change the current working directory using the command "**cd**" with the directory name as the first argument (any other arguments will be ignored by the interpreter).

Blank lines will be ignored by the interpreter, and comment lines (lines which starts with the character '#') will also not be interpreted by the itsh shell.

Commands separated by a '**;**' (semi-colon) are executed sequentially; the shell waits for each command to terminate in turn. The return status is the exit status of the last command executed.

Unfortunately, this shell does not support quoting in arguments, so it is not possible to use arguments that have whitespaces on them.
	
OPTIONS
-------

* Piping

A pipeline is a sequence of one or more commands separated by the '|' (pipe) character.
The format for a pipeline is:

**command1 [arg1 ...] | command2 [arg1 ...] | command3 [arg1 ...]**

The standard output of command1 is connected via a pipe to the standard input of command2, and the output of command3 is connected via a pipe to the standard input of the command3. The return status of a pipeline is the exit status of the last command. Each command in a pipeline is executed as a separate process.


* Input and/or Output Redirection

Before a command is executed, it is possible to redirect both input or output using a special notation interpreted by the shell. In order to redirect the input from a file, it is necessary to use the character '**<**' followed by a file name. On the other hand, for redirecting the output, it is necessary to use the character '**>**' followed by a file name. The following redirection operators may precede or appear anywhere within a simple command or may follow a command. Redirections are processed in the order they appear, from left to right. It is important to notice that while piping commands, just the first might have input redirection and the last may have it's output redirected.


* Background Jobs

Additionally, it is possible to execute a command or a set of commands in background using a special notation interpreted by the shell. In order to execute commands in the background, it is necessary to add the character '**&**' at the end of the command line. Thus, this character should be the last one in the command line, and if there is more than one command being piped, the background character just need to appear after the last one.


EXIT STATUS
-----------
The exit status of an executed command is the value returned by the waitpid system call or equivalent function. For the shell's purposes, a command which exits with a zero exit status has succeeded. An exit status of zero indicates success. A non-zero exit status indicates failure.

If a command fails because of an error during expansion or redirection, the exit status is going to be greater than zero.

Shell builtin commands return a status of 0 (true) if successful, and non-zero (false) if an error occurs while they execute.

This shell returns the exit status of the last command executed, unless a syntax error occurs, in which case it exits with a non-zero value.

ENVIRONMENT
-----------

This shell uses the execvp command from the exec family in order to execute the commands typed by the user. The search path is the path specified in the environment by the PATH variable. If this variable isn't specified, the default path ":/bin:/usr/bin" is used.

FILES
-----

**bin/itsh** - The itsh executable

**doc/itsh.1** - This man page


NOTES
-----

The MIT License (MIT)

Copyright (c) 2014 Alexandre Leites

Permission is hereby granted, free of charge, to any person obtaining a copy
of this software and associated documentation files (the "Software"), to deal
in the Software without restriction, including without limitation the rights
to use, copy, modify, merge, publish, distribute, sublicense, and/or sell
copies of the Software, and to permit persons to whom the Software is
furnished to do so, subject to the following conditions:

The above copyright notice and this permission notice shall be included in
all copies or substantial portions of the Software.

THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND, EXPRESS OR
IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF MERCHANTABILITY,
FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT. IN NO EVENT SHALL THE
AUTHORS OR COPYRIGHT HOLDERS BE LIABLE FOR ANY CLAIM, DAMAGES OR OTHER
LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE, ARISING FROM,
OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR OTHER DEALINGS IN
THE SOFTWARE.

BUGS
----

It is possible to put the character '**&**' on all commands in a pipe without displaying any error. However, just the last '**&**' will be considered as a background task flag.

EXAMPLE
-------

* Example 1: **mkdir foo; mkdir bar**

This example uses the character '**;**' to execute two commands independently. The first command will create a directory called 'foo', and the second one will create a directory called 'bar'. If the first command fails, the second still being executed.

* Example 2: **ls -l | grep foo**

This example is using the character '|' to pipe two commands execution. The first command will list the contents of the current directory, and the output will be passed to the grep command that will search for the pattern 'foo' and finally, the output will be directed to the **standard output**.

* Example 3: **ls -l | grep foo > output.txt**

This example is using the character '|' to pipe two commands execution. The first command will list the contents of the current directory, and the output will be passed to the grep command that will search for the pattern 'foo' and finally, the output will be directed to the file named **output.txt**.

* Example 4: **cat file1.txt | grep phone > phonelist.txt &**

This example is using the character '|' to pipe two commands execution. The first command will display the contents of the **file1.txt**, however, the output will be passed to the grep command that will search for the pattern 'phone' and the output will be directed to the file named **phonelist.txt*. Using the character '**&**', it tells the itsh to execute this task in background, so the user can type other commands while this task is executed behind the scenes. When finished, the shell will display the process id (pid) and exit status of this task.


AUTHOR
------

Alexandre Leites


SEE ALSO
--------

[The GNU Readline Library](http://cnswww.cns.cwru.edu/php/chet/readline/rltop.html)

sh(1), readline(3)
