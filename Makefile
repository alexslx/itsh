RM := rm -rf
CC := gcc 
LIBS := -lreadline
EXE := itsh

SOURCES := main.c job.c command.c terminal.c utils.c
OBJECTS := $(SOURCES:.c=.o)
DOC_DIR := doc/

all: itsh

%.o: %.c
	@echo 'Building file: $<'
	@echo 'Invoking: Compiler'
	$(CC) -O0 -c -g3 -o "$@" "$<"
	@echo 'Finished building: $<'
	@echo ' '

itsh: $(OBJECTS)
	@echo 'Building target: $@'
	@echo 'Invoking: Linker'
	$(CC) -o $(EXE) $(OBJECTS) $(LIBS)
	@echo 'Finished building target: $@'
	@echo ' '

clean:
	$(RM) $(OBJECTS)
	$(RM) $(EXE)
	$(RM) *.tar.gz
	@echo ' '

dist: clean
	@echo 'Generating tarball'
	@read -p "Enter Student Number: " studentid;tar -cvzf $$studentid.tar.gz *
	@echo ' '

test-clean:
	$(RM) myFile*
	$(RM) test2
	$(RM) testdir

test: clean test-clean all
	@echo 'Running test...'
	@echo 'Use make test-clean to cleanup results.'
	./itsh < filetest

test-file: clean test-clean all
	@echo 'Running file test...'
	@echo 'Use make test-clean to cleanup results.'
	./longtest

.PHONY: all clean dist test-clean test test-file
